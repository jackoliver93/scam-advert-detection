from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import relationship

from sqlalchemy.sql import func

from src import db

# class Image(db.Model):
    # __tablename__ = "images"
    
    # id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    # created_date = db.Column(db.DateTime, default=func.now(), nullable=False)
    # image = db.Column(db.Text, nullable=False)
    # processed_images = relationship("ProcessedImage")

class MLModel(db.Model):
    __tablename__  = "ml_models"
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    model_name = db.Column(db.String(32), nullable=False)
    description = db.Column(db.Text, nullable = False)
    binary = db.Column(db.LargeBinary, nullable = False)
    processed_images = relationship("ProcessedImage")
 
 
class ProcessedImage(db.Model):
    __tablename__ = "processed_images"
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    #image_id = db.Column(db.Integer, db.ForeignKey("images.id"), nullable=False)
    text = db.Column(db.Text, nullable=True)
    objects = db.Column(ARRAY(db.String), nullable=True)
    is_scam = db.Column(db.Boolean(), default=True, nullable=False)
    probability = db.Column(db.Float, nullable=False)
    model_id = db.Column(db.Integer, db.ForeignKey("ml_models.id"), nullable=False)
    
