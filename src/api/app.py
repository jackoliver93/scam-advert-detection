import flask
import requests

# import user-defined python modules for prediction
from inference import make_prediction
from utils import AttributeSelector, get_keras_model, get_sklearn_model


# initialize Flask application
app = flask.Flask(__name__)


@app.route("/predict", methods=["GET"])
def predict():
    # initialize the data dictionary that will be returned from the
    # view
    data = {
        "success": False,
        "Verdict": None,
        "Scam probability": None,
        "Key words detected": None,
        "Objects detected": None,
    }

    # ensure an image was properly uploaded to our endpoint
    if flask.request.method == "GET":
        if flask.request.files.get("image"):
            # read the image in PIL format
            image = flask.request.files["image"].read()
            verdict, prob, text, objects = make_prediction(
                image, object_recognition_model, classifier_model
            )
            data["Verdict"] = verdict
            data["Scam probability"] = round(prob, 2)
            data["Key words detected"] = text
            data["Objects detected"] = objects
            # indicate that the request was a success
            data["success"] = True
    # return the data dictionary as a JSON response
    return flask.jsonify(data)


if __name__ == "__main__":

    print("* Flask starting server...please wait until server has fully started")
    object_recognition_model = get_keras_model()
    classifier_model = get_sklearn_model()
    app.run(debug=True)
