import io
import re
import pickle
import numpy as np
import pandas as pd
import scipy
import ast
import string
import pytesseract

from numpy import expand_dims
from pathlib import Path
from nltk.corpus import stopwords
from nltk.corpus import words
from nltk.stem.wordnet import WordNetLemmatizer
from PIL import Image


Lem = WordNetLemmatizer()


def load_image(image_file):
    # return Image.open(str(image_file)) #reads direct from file
    return Image.open(io.BytesIO(image_file))  # reads image represented as byes


def image_to_text(image_object):
    """
    Uses pytesseract method image_to_string
    to extract text from image as strings.

    Inputs:
        image_object: an image object

    Returns:
        text: a string containing all detected text.
    """

    return pytesseract.image_to_string(image_object)


def clean_text(image_file):

    image_object = load_image(image_file)
    message = image_to_text(image_object)

    web_strings = ["https", "www", "com"]
    message = re.sub(
        "([^\x00-\x7F])+", " ", str(message)
    )  # remove any non-latin characters
    message = message.replace("\\n", " ")  # remove new line special characters
    message = message.translate(
        message.maketrans(string.punctuation, " " * len(string.punctuation))
    )  # map punctuation onto space
    message = re.sub(r"\b\w{1,3}\b", "", message)  # remove words length 3 or less
    message = [
        word.lower()
        for word in message.split()
        if word.lower() not in stopwords.words("english")
    ]
    message = [word for word in message if word not in web_strings]
    message = list(set(message))  # removes duplicate words
    lemmatized = [Lem.lemmatize(w) for w in message]
    return lemmatized
