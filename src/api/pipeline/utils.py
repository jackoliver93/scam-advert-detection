import pickle
from pathlib import Path
from sklearn.base import BaseEstimator, TransformerMixin
from tensorflow.keras.models import load_model


class AttributeSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names

    def fit(self, dataframe, y=None):
        return self

    def transform(self, dataframe):
        return dataframe[self.attribute_names]


# Relative paths to models, change as appropriate for different models.
keras_model_path = Path(__file__).parent / "../saved_models/YOLOv3_model.h5"
sklearn_model_path = (
    Path(__file__).parent / "../saved_models/voting_classifier_pipeline.pkl"
)


def get_keras_model(model_path=keras_model_path):
    global model
    model = load_model(
        model_path, compile=False
    )  # compile is set to False when loading pre-trained model
    return model


def get_sklearn_model(model_path=sklearn_model_path):
    return pickle.load(open(model_path, "rb"))
