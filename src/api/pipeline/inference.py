import pandas as pd
import numpy as np
import pickle
import scipy
import ast

from pathlib import Path
from nltk.corpus import stopwords
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import Normalizer, StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.pipeline import FeatureUnion
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import VotingClassifier
from tensorflow.keras.models import load_model

# import user-defined python modules for text processing and image recognition
import text_processing
import object_recognition


"""Define functions"""


def string_to_list(string):
    return ast.literal_eval(string)


def expand_dataframe(dataframe, cols=["ob_types", "ob_prob"]):

    df = dataframe.copy()
    for index, row in dataframe.iterrows():

        # top two lines are used when list is saved in string representation in a dataframe
        # obj_list = string_to_list(row[cols[0]])
        # prob_list = string_to_list(row[cols[1]])
        obj_list = row[cols[0]]
        prob_list = row[cols[1]]
        df[cols[1]].loc[index] = np.mean(np.asarray(prob_list))

        if not obj_list:
            continue

        for obj in obj_list:
            flag = True
            n_obj = obj_list.count(obj)
            if obj in df:
                df[obj].loc[index] = n_obj
                flag = False
            if flag:
                df[obj] = 0
                flag = False
            df[obj].loc[index] = n_obj

    return df


def make_prediction(image_file, object_recognition_model, classifier_model):

    """This extracts text and objects from image stored information in
    dataframe, applies sklearn transform pipeline and finally returns
    the procesed information ready for classification model"""

    verdict_dictionary = {
        0: "Legitimate",
        1: "Scam",
    }  # used to map model output to a string

    # list of all possible objects classifiers have been trained to recognise.
    labels = [
        "person",
        "chair",
        "tvmonitor",
        "pottedplant",
        "bottle",
        "tie",
        "car",
        "motorbike",
        "truck",
        "laptop",
        "cup",
        "vase",
        "book",
        "cell phone",
        "bed",
        "diningtable",
        "handbag",
        "boat",
        "bicycle",
        "aeroplane",
        "snowboard",
        "sofa",
        "parking meter",
        "bus",
        "clock",
        "wine glass",
        "dog",
        "orange",
        "surfboard",
        "umbrella",
        "bowl",
        "remote",
        "backpack",
        "stop sign",
        "knife",
        "banana",
        "bench",
        "sports ball",
        "broccoli",
        "carrot",
        "bird",
        "toothbrush",
        "apple",
        "oven",
        "cake",
        "spoon",
        "elephant",
        "cat",
        "kite",
        "skateboard",
        "suitcase",
    ]

    # extract data from image
    text = text_processing.clean_text(image_file)
    objects, probabilities = object_recognition.extract_objects(
        image_file, object_recognition_model
    )

    # store data in a dataframe
    data = {
        "text": str(text),
        "objects": objects,
        "n_objects": len(objects),
        "object_probs": probabilities,
    }
    dummy = {
        "text": str(["dummy_text"]),
        "objects": [],
        "n_objects": 0,
        "object_probs": [],
    }  # dummy row is included so image can be turned into a dataframe
    data = [data, dummy]
    df = pd.DataFrame(data)  # store in dataframe

    for col in labels:  # initialise counts of all objects to 0
        df[col] = 0
    df = expand_dataframe(
        df, cols=["objects", "object_probs"]
    )  # count each object in image
    df.drop(
        "objects", axis=1, inplace=True
    )  # column of lists of objects no longer needed, so it's dropped

    # define text and numerical features to use with scikit-learn pipeline and make predictions
    text_features = "text"
    numerical_features = list(df.drop("text", axis=1).columns)

    prediction = classifier_model.predict(df)[0]  # discard prediction on dummy row
    probability = classifier_model.predict_proba(df)[
        0, 1
    ]  # probability of being a scam (first row second column)

    return verdict_dictionary[prediction], probability, text, objects


if __name__ == "__main__":
