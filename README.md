TO DO (revisiting in Feb 2022)
 * Add database to store image processing output and predictions
 * Refactor Flask application to use blueprints
 * Package all pre-processing steps into a single pipline
 * Containerise and deploy
 * Update this README file to explain new API structure, and info on how the model works

# Scam Advertising Detection
***

This is a python library for detecting scam advertising online. It contains:
 - A text processing module to extract and clean text from advert images.
 - An object recognition module to identify common objects contained in an advert image. By default, the module uses a YOLOv3 model built in Keras. 
 - An inference module which builds a structured dataset from the text and objects extracted from an advert image and applies a pre-trained Scikit-learn pipeline to transfrom data and predict if the advert is a scam or not a scam.
 - A RESTful API written in Flask, to which a user can send advert images and receive a vertict, (Scam or Not scam), the probability that the advert is a scam, the key words which were detected in the image and the objects detected in the image. 
 - A module to download pre trained weights and build the object recognition model.

It is intended to be used with other programs, such as web scrapers which can collect advertisement images from media platforms and send them to the API contained within this library.  
***
## Configuration 

1) Clone the repository: 
```bash
git clone https://gitlab.com/jackoliver93/scam-advert-detection.git
```


2) Install the Tesseract OCR engine in your environment. For Linux run:


```bash
sudo apt-get install tesseract-ocr
```

For windows, the latest installers are found at https://github.com/UB-Mannheim/tesseract/wiki

3) Install requirements by running:

```
pip install -r requirements.txt
```

4) Next download the packages from NLTK which are required to process the text by running the following three commands:

``` 
python -m nltk.downloader words
python -m nltk.downloader stopwords
python -m nltk.downloader wordnet
```

5) Build the object recognition model. The model weights file is large and may take a while to download, they will be stored in *saved_models/model_weights*. The model itself will be saved to *saved_models*. This is done by running the following command:
``` 
python build_object_recognition_model.py
```
***
#### Test the library

The environment is now set up for usage. To test the configuration, first move to the directory *scam_ad_detection* and run: 

``` 
python app.py
``` 

Open a second terminal, navigate to *tests* and run:
``` 
python make_request.py
``` 

If the request is successful the following output will be printed to the terminal:

``` json
{'Key words detected': ['gordon', 'million', 'ferrari', 'deal', 'buy', 'ramsay'], 'Objects detected': ['car', 'car', 'person', 'truck', 'person', 'car', '
car'], 'Scam probability': 0.91, 'Verdict': 'Scam', 'success': True}
```




